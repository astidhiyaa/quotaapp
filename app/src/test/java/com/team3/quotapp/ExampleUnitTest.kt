package com.team3.quotapp

import com.team3.quotapp.rest.ApiClient
import com.team3.quotapp.rest.model.InOutRequest
import org.junit.Assert
import org.junit.Assert.*
import org.junit.Test
import org.junit.runner.RunWith

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {
//    @Test
//    fun addition_isCorrect() {
//        assertEquals(4, 2 + 2)
//    }

    @Test
    fun canLogout () {
        val api = ApiClient.retrofit()
        val response = api.logout(InOutRequest(noTelepon = "+628125521660",password = "Team3@Dana")).execute()
        assert(response.code().equals(200))
    }
}
