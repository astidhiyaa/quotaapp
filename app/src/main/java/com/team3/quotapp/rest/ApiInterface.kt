package com.team3.quotapp.rest

import com.team3.quotapp.rest.model.*
import com.team3.quotapp.rest.response.*
import retrofit2.Call
import retrofit2.http.*
import okhttp3.MultipartBody;


interface ApiInterface {
    @POST("api/auth/signup")
    fun registerUser(@Body user: UserModel?): Call<SignUpResponse?>?

    @POST("api/auth/confirmation-otp/{mobileNumber}/otp")
    fun verifyPhoneNumber(@Path("mobileNumber") mobileNumber: String, @Body otp: OTPModel?): Call<UserInfoResponse?>?

    @POST("api/auth/signin")
    fun login(@Body loginRequest: InOutRequest): Call<InOutResponse>

    @POST("api/auth/signout")
    fun logout(@Body logoutRequest: InOutRequest): Call<InOutResponse>

    @POST("api/auth/forgot-password")
    fun forgotPassword(@Body forgotRequest: ForgotPasswordRequest): Call<ForgotPasswordResponse>

    // App via otp
    @PUT("api/auth/confirmation-forgotPassword/{mobile_number}/otp")
    fun confirmForgotPassword(
        @Path("mobile_number") mobile_number: String,
        @Body forgotRequest: ConfirmForgotRequest
    ): Call<ConfirmForgotResponse>

    // App via email
    @PUT("api/auth/change-password")
    fun changePassword(@Body changeRequest: ChangePasswordRequest): Call<ChangePasswordResponse>

    @POST("/api/auth/reset-password-inapplication")
    fun resetPassword(@Body resetRequest: ForgotPasswordRequest): Call<ForgotPasswordResponse>

    @DELETE("/api/auth/delete-user/{mobileNumber}")
    fun deleteAccount(@Path("mobileNumber") phone: String): Call<DeleteAccountResponse>

    @PUT("/api/auth/edit-user/{mobileNumber}")
    fun editUser(
        @Path("mobileNumber") phone: String,
        @Body editUserRequest: EditUserRequest): Call<EditUserResponse>

    @POST("api/provider/cek-paket")
    fun getInternetDataList(@Body nomer_hp: InetDataModel): Call<InternetDataResponse>

    @POST("/api/transaksi/choice")
    fun chooseInetPackage(@Body chosenPackage: TransactionChosenModel): Call<TransactionResponseChosen>

    @POST("/api/transaksi/E-wallet")
    fun setWalletTransaction(@Body wallet: WalletModel): Call<PayNowResponse>

    @POST("/api/transaksi/virtual-account")
    fun setVATransaction (@Body va: VAModel): Call<PayNowResponse>

    @Multipart
    @POST("/api/transaksi-upload-photo/buktipembayaran")
    fun uploadVA(@Part file: MultipartBody.Part) : Call<SignUpResponse>

    @GET("api/transaksi/history/{mobileNumber}")
    fun getHistoryTransaction(@Path("mobileNumber") mobile_number: String): Call<HistoryResponse>
}