package com.team3.quotapp.rest.model

import com.google.gson.annotations.SerializedName

data class ChangePasswordRequest(
    @SerializedName("email")
    var email: String,
    @SerializedName("newPassword")
    var newPassword: String,
    @SerializedName("confirmPassword")
    var confirmPassword: String
)