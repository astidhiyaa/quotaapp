package com.team3.quotapp.rest.response

import com.google.gson.annotations.SerializedName

data class InOutResponse(
    @SerializedName("token")
    var token: String,
    @SerializedName("type")
    var type: String,
    @SerializedName("id")
    var id: Int,
    @SerializedName("username")
    var username: String,
    @SerializedName("email")
    var email: String,
    @SerializedName("saldo")
    var saldo: String,
    @SerializedName("message")
    var message: String?,
    @SerializedName("status")
    var status: String?
)