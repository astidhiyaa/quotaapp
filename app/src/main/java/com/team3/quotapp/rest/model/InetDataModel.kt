package com.team3.quotapp.rest.model

import com.google.gson.annotations.SerializedName

data class InetDataModel(

	@field:SerializedName("nomer_hp")
	val nomerHp: String? = null
)
