package com.team3.quotapp.rest.model

import com.google.gson.annotations.SerializedName

data class InOutRequest(
    @SerializedName("noTelepon")
    var noTelepon: String,
    @SerializedName("password")
    var password: String
)