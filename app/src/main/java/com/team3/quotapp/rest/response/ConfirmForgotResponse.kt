package com.team3.quotapp.rest.response

import com.google.gson.annotations.SerializedName

data class ConfirmForgotResponse(
    @SerializedName("id")
    var id: Int,
    @SerializedName("noTelepon")
    var noTelepon: String,
    @SerializedName("namaUser")
    var namaUser: String,
    @SerializedName("email")
    var email: String,
    @SerializedName("password")
    var password: String,
    @SerializedName("virtualAccount")
    var virtualAccount: String,
    @SerializedName("message")
    var message: String,
    @SerializedName("status")
    var status: String,
    @SerializedName("saldo")
    var saldo: String,
    @SerializedName("createdDate")
    var createdDate: String,
    @SerializedName("updatedDate")
    var updatedDate: String
)