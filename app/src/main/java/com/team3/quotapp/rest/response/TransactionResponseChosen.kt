package com.team3.quotapp.rest.response

import com.google.gson.annotations.SerializedName

data class TransactionResponseChosen( //choice

    @field:SerializedName("statusTransaksi")
    val statusTransaksi: Boolean? = null,

    @field:SerializedName("harga")
    val harga: Int? = null,

    @field:SerializedName("noTelepon")
    val noTelepon: String? = null,

    @field:SerializedName("namaProvider")
    val namaProvider: String? = null,

    @field:SerializedName("waktuTransaksi")
    val waktuTransaksi: String? = null,

    @field:SerializedName("paketData")
    val paketData: String? = null,

    @field:SerializedName("id")
    val id: Int? = null,

    @field:SerializedName("nomorPaketData")
    val nomorPaketData: String? = null
)

data class PayNowResponse( //E-wallet &VA response

    @field:SerializedName("pembayaranMelalui")
    val pembayaranMelalui: String? = null,

    @field:SerializedName("idtransaksi")
    val idtransaksi: Int? = null,

    @field:SerializedName("harga")
    val harga: Int? = null,

    @field:SerializedName("saldoAkhir")
    val saldoAkhir: Int? = null,

    @field:SerializedName("namaProvider")
    val namaProvider: String? = null,

    @field:SerializedName("statusPembayaran")
    val statusPembayaran: Boolean? = null,

    @field:SerializedName("paketData")
    val paketData: String? = null,

    @field:SerializedName("tanggal")
    val tanggal: String? = null,

    @field:SerializedName("nomorPaketData")
    val nomorPaketData: String? = null,

    @field:SerializedName("namaUser")
    val namaUser: String? = null,

    @field:SerializedName("nomorTeleponUser")
    val nomorTeleponUser: String? = null,

    @field:SerializedName("message")
    val message: String? = null,

    @field:SerializedName("status")
    var status: String? = null
)
