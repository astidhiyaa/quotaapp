package com.team3.quotapp.rest.model

import com.google.gson.annotations.SerializedName

data class HistoryRequest(

	@field:SerializedName("email")
	val email: String? = null
)
