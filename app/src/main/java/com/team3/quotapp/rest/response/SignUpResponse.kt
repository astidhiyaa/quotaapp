package com.team3.quotapp.rest.response

import com.google.gson.annotations.SerializedName

data class SignUpResponse(

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
    var status: String? = null
)
