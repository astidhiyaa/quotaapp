package com.team3.quotapp.rest.response

import com.google.gson.annotations.SerializedName

data class UserInfoResponse(

	@field:SerializedName("password")
	val password: String? = null,

	@field:SerializedName("createdDate")
	val createdDate: String? = null,

	@field:SerializedName("noTelepon")
	val noTelepon: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("saldo")
	val saldo: Int? = null,

	@field:SerializedName("updatedDate")
	val updatedDate: String? = null,

	@field:SerializedName("message")
	var message: String? = null,

	@field:SerializedName("virtualAccount")
	val virtualAccount: String? = null,

	@field:SerializedName("email")
	val email: String? = null,

	@field:SerializedName("namaUser")
	val namaUser: String? = null,

	@field:SerializedName("status")
    var status: String? = null
)
