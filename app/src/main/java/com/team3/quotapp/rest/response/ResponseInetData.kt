package com.team3.quotapp.rest.response

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class InternetDataResponse(
	@field:SerializedName("paketData")
	val paketData: List<InternetDataList?>? = null
)
data class InternetDataList(
	@field:SerializedName("nama_provider")
	val namaProvider: String? = null,

	@field:SerializedName("harga")
	val harga: String? = null,

	@field:SerializedName("id")
	val id: String? = null,

	@field:SerializedName("paket_data")
	val paketData: String? = null
): Parcelable {
	constructor(parcel: Parcel) : this(
		parcel.readString(),
		parcel.readString(),
		parcel.readString(),
		parcel.readString()
	)

    override fun writeToParcel(parcel: Parcel, flags: Int) {
		parcel.writeString(namaProvider)
		parcel.writeString(harga)
		parcel.writeString(id)
		parcel.writeString(paketData)
	}

	override fun describeContents(): Int {
		return 0
	}

	companion object CREATOR : Parcelable.Creator<InternetDataList> {
		override fun createFromParcel(parcel: Parcel): InternetDataList {
			return InternetDataList(parcel)
		}

		override fun newArray(size: Int): Array<InternetDataList?> {
			return arrayOfNulls(size)
		}
	}
}
