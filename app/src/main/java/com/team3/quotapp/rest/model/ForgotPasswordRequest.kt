package com.team3.quotapp.rest.model

import com.google.gson.annotations.SerializedName

data class ForgotPasswordRequest(
    @SerializedName("email")
    var email: String,
    @SerializedName("noTelepon")
    var noTelepon: String
)