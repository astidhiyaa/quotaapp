package com.team3.quotapp.rest.model

import com.google.gson.annotations.SerializedName

data class OTPModel(
    @field:SerializedName("codeOtp")
    val codeOtp: String? = null,

    @field:SerializedName("statusOtp")
    val statusOtp: String? = null
)