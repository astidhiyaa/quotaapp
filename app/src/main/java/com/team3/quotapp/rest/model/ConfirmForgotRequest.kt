package com.team3.quotapp.rest.model

import com.google.gson.annotations.SerializedName

data class ConfirmForgotRequest(
    @SerializedName("otp")
    var otp: String,
    @SerializedName("statusOtp")
    var statusOtp: String,
    @SerializedName("newPassword")
    var newPassword: String,
    @SerializedName("confirmPassword")
    var confirmPassword: String
)