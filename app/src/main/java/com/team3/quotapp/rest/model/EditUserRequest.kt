package com.team3.quotapp.rest.model

import com.google.gson.annotations.SerializedName

data class EditUserRequest(
    @SerializedName("namaUser")
    var namaUser: String,
    @SerializedName("noTelepon")
    var noTelepon: String,
    @SerializedName("email")
    var email: String
)