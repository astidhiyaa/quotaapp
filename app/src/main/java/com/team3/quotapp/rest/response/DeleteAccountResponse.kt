package com.team3.quotapp.rest.response

import com.google.gson.annotations.SerializedName

data class DeleteAccountResponse(
    @SerializedName("message")
    var message: String,
    @SerializedName("status")
    var status: String
)