package com.team3.quotapp.rest.model

data class TransactionChosenModel( //choice model
    val namaProvider: String? = null,
    val harga: Int? = null,
    val paketData: String? = null,
    val noTelepon: String? = null,
    val nomorPaketData: String? = null
)
data class WalletModel( //E-wallet model
    val noTelepon: String? = null,
    val pinTransaksi: Int? = null
)
data class VAModel( //Va model
    val noTelepon: String? = null,
    val virtualAccount: Long? = null
)


