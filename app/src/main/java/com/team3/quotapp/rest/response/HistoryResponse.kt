package com.team3.quotapp.rest.response

import com.google.gson.annotations.SerializedName

data class HistoryResponse(
    @SerializedName("history")
    var historyList: List<HistoryList>? = null)

data class HistoryList(
    @SerializedName("idtransaksi")
    var idtransaksi: Int,
    @SerializedName("namaUser")
    var namaUser: String,
    @SerializedName("nomorTeleponUser")
    var nomorTeleponUser: String,
    @SerializedName("nomorPaketData")
    var nomorPaketData: String,
    @SerializedName("namaProvider")
    var namaProvider: String,
    @SerializedName("paketData")
    var paketData: String,
    @SerializedName("harga")
    var harga: Double,
    @SerializedName("tanggal")
    var tanggal: String,
    @SerializedName("pembayaranMelalui")
    var pembayaranMelalui: String,
    @SerializedName("statusPembayaran")
    var statusPembayaran: String,
    @SerializedName("saldoAkhir")
    var saldoAkhir: String,
    @SerializedName("statusUpload")
    var statusUpload: String
)