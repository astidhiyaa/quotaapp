package com.team3.quotapp.data.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query

@Dao
interface HistoryDao {
    @Insert
    fun insert(news: HistoryEntity)

    @Query("DELETE FROM history_table")
    fun deleteAllHistory()

    @Query("SELECT * FROM history_table")
    fun getAllHistory(): LiveData<List<HistoryEntity>>
}

