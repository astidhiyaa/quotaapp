package com.team3.quotapp.data.db

import androidx.room.*

@Entity(tableName = "history_table")
data class HistoryEntity (
    var username: String,
    var nomer: String,
    var provider: String,
    var paket: String,
    var harga: Int,
    var tanggal: String
) {
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0
}