package com.team3.quotapp.di

import android.app.Application
import com.team3.quotapp.data.db.AppDatabase
import com.team3.quotapp.data.db.HistoryDao
import com.team3.quotapp.repositories.HistoryRepository
import com.team3.quotapp.repositories.QuotaRepository
import com.team3.quotapp.rest.ApiClient
import com.team3.quotapp.ui.adapters.FailedHistoryAdapter
import com.team3.quotapp.ui.adapters.InternetDataAdapter
import com.team3.quotapp.ui.adapters.SuccessHistoryAdapter
import com.team3.quotapp.viewmodel.HistoryViewModel
import com.team3.quotapp.viewmodel.QuotaViewModel
import org.koin.android.ext.koin.androidContext
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.core.context.startKoin
import org.koin.dsl.module
class KoinModules : Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(applicationContext)
            modules(listOf(restModule,dbModule, repositoryModule, uiModule))
        }
    }

    val restModule = module {
        single { ApiClient }
        single {
            ApiClient.retrofit()
        }
    }

    val dbModule = module {
        single {
            AppDatabase.getInstance(context = get())
        }
        factory { get<AppDatabase>().historyDao() }
    }

    val repositoryModule = module {
        single { QuotaRepository(get()) }
        fun provideRepo(dao: HistoryDao, api: ApiClient): HistoryRepository {
            return HistoryRepository(dao, api)
        }
        single { provideRepo(get(), get()) }
    }

    val uiModule = module {
        factory { InternetDataAdapter() }
        viewModel { QuotaViewModel(get()) }
        factory { FailedHistoryAdapter() }
        factory { SuccessHistoryAdapter() }
        viewModel { HistoryViewModel(get()) }
    }
}