package com.team3.quotapp.ui.activities

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.team3.quotapp.R
import com.team3.quotapp.preference.PreferenceManager
import com.team3.quotapp.rest.model.ConfirmForgotRequest
import com.team3.quotapp.rest.response.ConfirmForgotResponse
import com.team3.quotapp.ui.fragments.CustomProgressDialog
import com.team3.quotapp.viewmodel.QuotaViewModel
import kotlinx.android.synthetic.main.activity_change_password_otp.*
import org.koin.android.ext.android.inject

class ChangePasswordOtpActivity : AppCompatActivity() {
    private val quotaViewModel: QuotaViewModel by inject()
    private lateinit var preferenceManager: PreferenceManager
    private val progressDialog = CustomProgressDialog()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_change_password_otp)
        setupToolbar()
        onClick()
    }

    private fun setupToolbar() {
        setSupportActionBar(toolbar)
        assert(supportActionBar != null)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_keyboard_arrow_left_grey_24dp)
        supportActionBar?.title = ""
        toolbar.setNavigationOnClickListener { view: View? -> onBackPressed() }
    }

    private fun onClick() {
        btn_change_password.setOnClickListener {
            progressDialog.show(this,"Please Wait...")
            initData()
        }
    }

    private fun initData() {
        val otp = et_pin1.text.toString() + et_pin2.text.toString() + et_pin3.text.toString() + et_pin4.text.toString()
        val status = "true"
        val password = et_password.text.toString()
        val confirmPassword = et_confirm_password.text.toString()
        val preference = getSharedPreferences("QuotaApp", 0)
        val phone = preference.getString("PREF_PHONE", "").toString()

        confirmForgotPassword(phone, otp, status, password, confirmPassword)
    }

    private fun setLogIn(data: ConfirmForgotResponse) {
        preferenceManager = PreferenceManager(this)
        preferenceManager.setConfirmForgot(data)
    }

    private fun confirmForgotPassword(phone: String, otp: String, status: String, newPassword: String, confirmPassword: String) {
        quotaViewModel.confirmForgotPassword(phone,
            ConfirmForgotRequest(otp, status, newPassword, confirmPassword))
            .observe(this, Observer<ConfirmForgotResponse> {
                if (it?.status.equals("200")) {
                    progressDialog.dialog.dismiss()
                    setLogIn(it)
                    preferenceManager.setLoggedIn(true)
                    val preference = getSharedPreferences("QuotaApp", 0)
                    val editor = preference.edit()
                    editor.putString("PREF_PASSWORD", et_password.text.toString().trim())
                    editor.apply()
                    startActivity(Intent(this, MainActivity::class.java))
                } else {
                    progressDialog.dialog.dismiss()
                    Toast.makeText(applicationContext, "Something went wrong! ${it.message}",
                        Toast.LENGTH_LONG).show()
                }
            })
    }
}
