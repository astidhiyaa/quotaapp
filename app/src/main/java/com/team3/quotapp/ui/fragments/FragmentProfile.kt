package com.team3.quotapp.ui.fragments

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.Observer
import com.team3.quotapp.R
import com.team3.quotapp.preference.PreferenceManager
import com.team3.quotapp.rest.model.InOutRequest
import com.team3.quotapp.rest.response.InOutResponse
import com.team3.quotapp.ui.activities.*
import com.team3.quotapp.viewmodel.QuotaViewModel
import kotlinx.android.synthetic.main.fragment_profile.*
import org.koin.android.ext.android.inject

class FragmentProfile : Fragment() {
    private val quotaViewModel: QuotaViewModel by inject()
    private lateinit var preferenceManager: PreferenceManager

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_profile, container, false)

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        preferenceManager = PreferenceManager(activity!!.applicationContext)
        initView()
        onClickGroup()
    }

    private fun initView() {
        val name = preferenceManager.getUserData().username
        tv_profileName.text = name
    }

    private fun onClickGroup() {
        profile_settings_account.setOnClickListener {
            val intent = Intent(activity, AccountDetailActivity::class.java)
            startActivity(intent)
        }

        profile_settings_change_name.setOnClickListener {
            val intent = Intent(activity, ChangeUserActivity::class.java)
            startActivity(intent)
        }

        security_settings_change_password.setOnClickListener {
            val intent = Intent(activity, ResetPasswordActivity::class.java)
            activity!!.startActivity(intent)
        }

        security_settings_delete_account.setOnClickListener {
            val intent = Intent(activity, DeleteAccountActivity::class.java)
            activity!!.startActivity(intent)
        }

        btn_logout.setOnClickListener {
            initLogOut()
        }
    }

    private fun initLogOut() {
        val preference = activity!!.getSharedPreferences("QuotaApp", 0)
        val phone = preference.getString("PREF_PHONE", "").toString()
        val password = preference.getString("PREF_PASSWORD", "").toString()
        logout(phone, password)
    }

    private fun logout(phone: String, pass: String) {
        quotaViewModel.logout(InOutRequest(phone, pass)).observe(this, Observer<InOutResponse> {
            if (it.status == "200") {
                preferenceManager.setLoggedIn(false)
                val intent = Intent(activity, LoginActivity::class.java)
                activity!!.startActivity(intent)
            } else {
                Toast.makeText(
                    activity, "Something went wrong! ${it.message}", Toast.LENGTH_LONG
                ).show()
            }
        })
    }
}
