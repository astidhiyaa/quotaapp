package com.team3.quotapp.ui.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import com.team3.quotapp.R
import com.team3.quotapp.preference.PreferenceManager
import com.team3.quotapp.rest.response.DeleteAccountResponse
import com.team3.quotapp.viewmodel.QuotaViewModel
import kotlinx.android.synthetic.main.activity_delete_account.*
import org.koin.android.ext.android.inject

class DeleteAccountActivity : AppCompatActivity() {
    private val quotaViewModel: QuotaViewModel by inject()
    private lateinit var preferenceManager: PreferenceManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_delete_account)
        setupToolbar()
        onClickGroup()
    }

    private fun setupToolbar() {
        setSupportActionBar(toolbar)
        assert(supportActionBar != null)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_keyboard_arrow_left_grey_24dp)
        supportActionBar?.title = ""
        toolbar.setNavigationOnClickListener { view: View? -> onBackPressed() }
    }

    private fun onClickGroup() {
        btn_delete_account.setOnClickListener {
            iniDeleteAccount()
        }

        btn_cancel.setOnClickListener {
            onBackPressed()
        }
    }

    private fun iniDeleteAccount() {
        val preference = getSharedPreferences("QuotaApp", 0)
        val phone = preference.getString("PREF_PHONE", "").toString()
        deleteAccount(phone)
    }

    private fun deleteAccount(phone: String) {
        quotaViewModel.deleteAccount(phone).observe(this, Observer<DeleteAccountResponse> {
            if (it.status == "200") {
                preferenceManager = PreferenceManager(this)
                preferenceManager.clearSharedPreference()
                val intent = Intent(this, GetStartedActivity::class.java)
                startActivity(intent)
            } else {
                Toast.makeText(
                    this, "Something went wrong! ${it.message}", Toast.LENGTH_LONG
                ).show()
            }
        })
    }
}
