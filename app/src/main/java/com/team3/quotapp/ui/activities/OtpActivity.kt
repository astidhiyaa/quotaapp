package com.team3.quotapp.ui.activities

import android.content.Intent
import android.os.Bundle
import android.os.CountDownTimer
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.team3.quotapp.R
import com.team3.quotapp.rest.model.OTPModel
import com.team3.quotapp.rest.model.UserModel
import com.team3.quotapp.rest.response.SignUpResponse
import com.team3.quotapp.rest.response.UserInfoResponse
import com.team3.quotapp.ui.fragments.CustomProgressDialog
import com.team3.quotapp.viewmodel.QuotaViewModel
import kotlinx.android.synthetic.main.activity_otp.*
import org.koin.android.ext.android.inject
import java.util.concurrent.TimeUnit

class OtpActivity : AppCompatActivity() {
    private val quotaViewModel: QuotaViewModel by inject()
    private val progressDialog = CustomProgressDialog()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_otp)
        setupToolbar()
        setCountDown()
        verifyOtp()
    }

    private fun setupToolbar(){
        setSupportActionBar(toolbar)
        assert(supportActionBar != null)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_keyboard_arrow_left_grey_24dp)
        supportActionBar?.title = ""
        tv_verification_phone.text = intent.extras?.getString("mobileNumber")
    }

    private fun setCountDown() {
        val countDownTimer = object : CountDownTimer(180000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                tv_resend.text = "Resend code in ${String.format(
                    "%d : %d",
                    TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished),
                    TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) - TimeUnit.MINUTES.toSeconds(
                        TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished)
                    )
                )}"
            }

            override fun onFinish() {
                tv_resend.text = "Please click to resend code!"
                tv_resend.setOnClickListener {
                    var userInfo = intent.extras?.getParcelable<UserModel>("value")
                    resendCode(userInfo)
                }
            }
        }
        countDownTimer.start()
    }

    fun resendCode(userModel: UserModel?){
        quotaViewModel.registerUser(userModel!!).observe(this, Observer<SignUpResponse>{
            if(it.status != "200"){
                Toast.makeText(this, "Something went wrong! ${it.message}", Toast.LENGTH_LONG).show()
            }
        })
    }
    private fun verifyOtp() {
        val mobileNumber = intent.extras?.getString("mobileNumber")
        btn_continue.setOnClickListener {
            progressDialog.show(this,"Please Wait...")
            val otp = et_pin1.text.toString() + et_pin2.text.toString() + et_pin3.text.toString() + et_pin4.text.toString()
            quotaViewModel.verifyPhoneNumber(mobileNumber!!, OTPModel(otp,statusOtp = "true")).observe(this, Observer<UserInfoResponse>{
                if (it.status == "200") {
                    progressDialog.dialog.dismiss()
                    val intent = Intent(this, SuccessOtpActivity::class.java)
                    intent.putExtra("mobileNumber", mobileNumber)
                    startActivity(intent)
                } else {
                    progressDialog.dialog.dismiss()
                    Toast.makeText(this, "Something went wrong! ${it.message}",
                        Toast.LENGTH_LONG).show()
                }
            })
        }
    }
}
