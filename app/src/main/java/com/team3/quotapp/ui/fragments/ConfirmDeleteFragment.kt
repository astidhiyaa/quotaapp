package com.team3.quotapp.ui.fragments

import android.app.AlertDialog
import android.app.Dialog
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import android.widget.Toast
import androidx.annotation.Nullable
import androidx.fragment.app.DialogFragment

import com.team3.quotapp.R
import com.team3.quotapp.preference.PreferenceManager
import com.team3.quotapp.rest.response.DeleteAccountResponse
import com.team3.quotapp.ui.activities.GetStartedActivity
import com.team3.quotapp.viewmodel.QuotaViewModel
import org.koin.android.ext.android.inject

class ConfirmDeleteFragment : DialogFragment() {
    private val quotaViewModel: QuotaViewModel by inject()
    private lateinit var preferenceManager: PreferenceManager

    override fun onCreateDialog(@Nullable savedInstanceState: Bundle?): Dialog {
        if (arguments != null) {
            if (arguments!!.getBoolean("notAlertDialog")) {
                return super.onCreateDialog(savedInstanceState)
            }
        }
        val builder: AlertDialog.Builder = AlertDialog.Builder(activity)
        builder.setTitle("Delete account")
        builder.setMessage("Are you sure to delete this account")
        builder.setPositiveButton("Ok", DialogInterface.OnClickListener { dialog, which ->
            iniDeleteAccount()
        })
        builder.setNegativeButton("Cancel") { _, _ ->
            dismiss()
        }
        return builder.create()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_confirm_delete, container, false)
    }

    private fun iniDeleteAccount() {
        val preference = activity!!.getSharedPreferences("QuotaApp", 0)
        val phone = preference.getString("PREF_PHONE", "").toString()
        deleteAccount(phone)
    }

    private fun deleteAccount(phone: String) {
        quotaViewModel.deleteAccount(phone).observe(this, Observer<DeleteAccountResponse> {
            if (it.status == "200") {
                preferenceManager.clearSharedPreference()
                val intent = Intent(activity, GetStartedActivity::class.java)
                activity!!.startActivity(intent)
            } else {
                Toast.makeText(
                    activity, "Something went wrong! ${it.message}", Toast.LENGTH_LONG
                ).show()
            }
        })
    }
}
