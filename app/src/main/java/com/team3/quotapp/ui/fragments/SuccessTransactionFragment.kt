package com.team3.quotapp.ui.fragments

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.team3.quotapp.R
import com.team3.quotapp.rest.ApiClient
import com.team3.quotapp.rest.response.HistoryList
import com.team3.quotapp.rest.response.HistoryResponse
import com.team3.quotapp.ui.adapters.SuccessHistoryAdapter
import com.team3.quotapp.viewmodel.HistoryViewModel
import kotlinx.android.synthetic.main.fragment_success_transaction.*
import org.koin.android.ext.android.inject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SuccessTransactionFragment : Fragment() {
    private val adapter: SuccessHistoryAdapter by inject()
    private val historyViewModel: HistoryViewModel by inject()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_success_transaction, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initData()
    }

    private fun initData() {
        val preference = activity!!.getSharedPreferences("QuotaApp", 0)
        val phone = preference.getString("PREF_PHONE","").toString()
        getHistory()
        setupRecyclerView()
    }

    private fun getHistory() {
        val preference = this.activity!!.getSharedPreferences("QuotaApp", 0)
        val phone = preference.getString("PREF_PHONE", "").toString()
        historyViewModel.getHistory(phone).observe(this, Observer<List<HistoryList>>{
            list -> list?.let {
                if(it.isNotEmpty())
                adapter.setHistory(it)
            }
        })
    }

    private fun setupRecyclerView() {
        rv_history.layoutManager = LinearLayoutManager(activity)
        rv_history.setHasFixedSize(true)
        rv_history.adapter = adapter
    }
}
