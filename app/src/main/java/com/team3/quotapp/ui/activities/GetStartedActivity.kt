package com.team3.quotapp.ui.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.team3.quotapp.R
import com.team3.quotapp.preference.PreferenceManager
import com.team3.quotapp.viewmodel.QuotaViewModel
import kotlinx.android.synthetic.main.activity_get_started.*
import org.koin.android.ext.android.inject

class GetStartedActivity : AppCompatActivity() {
    private val quotaViewModel: QuotaViewModel by inject()
    private lateinit var preferenceManager: PreferenceManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_get_started)
        onClickGroup()
        isLogIn()
    }

    private fun onClickGroup() {
        btn_create_account.setOnClickListener {
            val intent = Intent(this, RegisterActivity::class.java)
            startActivity(intent)
        }

        btn_login_account.setOnClickListener {
            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
        }
    }

    private fun isLogIn() {
        preferenceManager = PreferenceManager(this)
        if (preferenceManager.isLoggedIn()) {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }
    }
}
