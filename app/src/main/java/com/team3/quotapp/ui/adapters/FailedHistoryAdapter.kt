package com.team3.quotapp.ui.adapters

import android.content.Context
import android.graphics.drawable.Drawable
import android.text.SpannableString
import android.text.TextUtils
import android.text.style.RelativeSizeSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.team3.quotapp.R
import com.team3.quotapp.data.db.HistoryEntity

class FailedHistoryAdapter : RecyclerView.Adapter<FailedHistoryAdapter.HistoryHolder>() {
    private var historyEntityList: List<HistoryEntity> = ArrayList()
    private var mContext: Context? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HistoryHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_history, parent, false)
        mContext = parent.context
        return HistoryHolder(itemView)
    }

    override fun onBindViewHolder(holder: HistoryHolder, position: Int) {
        val currentHistory = historyEntityList[position]
        holder.nameTextView.text = currentHistory.paket
        holder.dateTextView.text = customDate(currentHistory.tanggal)
        holder.priceTextView.text = customPrice(currentHistory.harga.toString())
        holder.operatorImageView.setImageDrawable(initView())
    }

    override fun getItemCount(): Int {
        return historyEntityList.size
    }

    fun setHistory(pulseData: List<HistoryEntity>) {
        this.historyEntityList = pulseData
        notifyDataSetChanged()
    }

    inner class HistoryHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var operatorImageView: ImageView = itemView.findViewById(R.id.iv_operator)
        var nameTextView: TextView = itemView.findViewById(R.id.tv_package_name)
        var dateTextView: TextView = itemView.findViewById(R.id.tv_transaction_date)
        var priceTextView: TextView = itemView.findViewById(R.id.tv_package_price)
    }

    private fun customDate(date: String): String {
        val text = date.take(10)
        return text
    }

    private fun customPrice(price: String): String {
        val text = price.replace(".0","")
        val text2 = "Rp ${text}"
        return text2
    }

    private fun initView(): Drawable {
        val preference = mContext!!.getSharedPreferences("QuotaApp", 0)
        val phone = preference.getString("PREF_LAST_PROVIDER", "").toString()
        val number = phone.take(6)
        return when (number) {
            "+62851", "+62852", "+62853", "+62823" -> mContext!!.resources.getDrawable(R.drawable.logo_telkomsel)
            "+62831", "+62832", "+62833", "+62838" -> mContext!!.resources.getDrawable(R.drawable.logo_axis)
            "+62856", "+62857" -> mContext!!.resources.getDrawable(R.drawable.logo_indosat)
            "+62815", "+62816", "+62858" -> mContext!!.resources.getDrawable(R.drawable.logo_indosat)
            "+62812", "+62813", "+62821", "+62822" -> mContext!!.resources.getDrawable(R.drawable.logo_telkomsel)
            "+62881", "+62882", "+62883", "+62884", "+62885", "+62886", "+62887", "+62888", "+62889" -> mContext!!.resources.getDrawable(
                R.drawable.logo_telkomsel
            )
            "+62895", "+62896", "+62897", "+62898", "+62899" -> mContext!!.resources.getDrawable(R.drawable.logo_three)
            "+62817", "+62818", "+62819", "+62859", "+62877", "+62878" -> mContext!!.resources.getDrawable(
                R.drawable.logo_xl
            )
            else -> mContext!!.resources.getDrawable(R.drawable.ic_close_black_24dp)
        }
    }
}
