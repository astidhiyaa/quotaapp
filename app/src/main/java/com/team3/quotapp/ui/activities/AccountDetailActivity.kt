package com.team3.quotapp.ui.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.InputType
import android.view.View
import com.team3.quotapp.R
import kotlinx.android.synthetic.main.activity_account_detail.*

class AccountDetailActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_account_detail)
        setupToolbar()
        initView()
    }

    private fun setupToolbar() {
        setSupportActionBar(toolbar)
        assert(supportActionBar != null)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_keyboard_arrow_left_grey_24dp)
        supportActionBar?.title = ""
        toolbar.setNavigationOnClickListener { view: View? -> onBackPressed() }
    }

    private fun initView() {
        val pref = getSharedPreferences("QuotaApp",0)
        val phone = pref.getString("PREF_PHONE", "")
        val username = pref.getString("PREF_NAME", "")
        val email = pref.getString("PREF_EMAIL", "")

        et_phone.setText(phone)
        et_phone.inputType = InputType.TYPE_NULL;
        et_name.setText(username)
        et_name.inputType = InputType.TYPE_NULL;
        et_email.setText(email)
        et_email.inputType = InputType.TYPE_NULL;

        btn_done.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }
    }
}
