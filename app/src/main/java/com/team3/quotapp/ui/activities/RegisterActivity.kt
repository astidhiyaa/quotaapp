package com.team3.quotapp.ui.activities

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.Observer
import com.team3.quotapp.R
import com.team3.quotapp.rest.model.UserModel
import com.team3.quotapp.rest.response.SignUpResponse
import com.team3.quotapp.ui.fragments.CustomProgressDialog
import com.team3.quotapp.ui.fragments.FragmentVerifyRegister
import com.team3.quotapp.viewmodel.QuotaViewModel
import kotlinx.android.synthetic.main.activity_register.*
import org.koin.android.ext.android.inject
import java.util.regex.Pattern

class RegisterActivity : AppCompatActivity() {
    private val quotaViewModel: QuotaViewModel by inject()
    private val progressDialog = CustomProgressDialog()
    val emailRegex =
        "^([A-Za-z][A-Za-z0-9\\-\\.\\_]*)\\@([A-Za-z][A-Za-z0-9\\-\\_]*)(\\.[A-Za-z][A-Za-z0-9\\-\\_]*)+\$"
    val passwordRegex = "(?=.*[!@#\$%^&*])(?=.*[0-9])(?=.*[A-Z])(?=.*[0-9])^.{8,16}\$"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        setupToolbar()
        onClick()
        validateForm()
        registerUser()
    }

    private fun setupToolbar() {
        setSupportActionBar(toolbar)
        assert(supportActionBar != null)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_keyboard_arrow_left_grey_24dp)
        supportActionBar?.title = ""
    }

    private fun onClick() {
        tv_already_have_account.setOnClickListener {
            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
        }
    }

    private fun validateForm(){
        et_full_name.addTextChangedListener(object : TextWatcher{
            override fun afterTextChanged(p0: Editable?) {
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if(et_full_name.text?.length!! < 3 ||  et_full_name.text?.length!! > 20) layout_name.error ="Min. 3 chars & max 20 chars!"
                else layout_name.error = null
            }
        })

        et_email.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if (!Pattern.matches(emailRegex, et_email.text.toString())) {
                    layout_email.error = "E-mail is invalid!"
                } else {
                    layout_email.error = null
                }
            }
        })

        et_password.addTextChangedListener(object : TextWatcher{
            override fun afterTextChanged(p0: Editable?) {
            }
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if(!Pattern.matches(passwordRegex,et_password.text)) layout_password.error ="8-16 chars, must contain 1 lowercase alphabetical character, 1 uppercase alphabetical character, 1 numeric character"
                else layout_password.error = null
            }
        })

        et_confirmpassword.addTextChangedListener(object :TextWatcher{
            override fun afterTextChanged(p0: Editable?) {
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if(et_confirmpassword.text.toString() != et_password.text.toString()) layout_confirmpassword.error ="Password must be the same!"
                else layout_confirmpassword.error = null
            }

        })

        et_pin.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if (et_pin.text?.length!! < 6) layout_pin.error = "Please enter 6 digit PIN code!"
                else layout_pin.error = null
            }

        })
    }

    private fun registerUser() {
        btn_register.setOnClickListener {
            progressDialog.show(this, "Please Wait...")
            val phone = "+62${etPhoneRegister.text}"
            if (Pattern.matches(emailRegex, et_email.text.toString())
                && Pattern.matches(passwordRegex, et_password.text)
                && et_confirmpassword.text.toString() == et_password.text.toString()
            ) {
                var userInfo = UserModel(
                    noTelepon = phone,
                    email = et_email.text.toString(),
                    password = et_password.text.toString(),
                    confirmPassword = et_confirmpassword.text.toString(),
                    namaUser = et_full_name.text.toString(),
                    pinTransaksi = et_pin.text.toString().toInt()
                )
                quotaViewModel.registerUser(userInfo)
                    .observe(this, Observer<SignUpResponse> {
                        if (it.status == "200"){
                            progressDialog.dialog.dismiss()
                            val dialogFragment = FragmentVerifyRegister()
                            val bundle = Bundle()
                            bundle.putParcelable("value", userInfo)
                            bundle.putString("mobileNumber", phone)
                            dialogFragment.arguments = bundle
                            val ft: FragmentTransaction = supportFragmentManager.beginTransaction()
                            dialogFragment.show(ft,"exampleBottomSheet")
                        }
                        else {
                            progressDialog.dialog.dismiss()
                            Toast.makeText(this, "Something went wrong! ${it.message}", Toast.LENGTH_LONG).show()
                        }
                    })
            } else {
                progressDialog.dialog.dismiss()
                Toast.makeText(this, "Please check your input again!", Toast.LENGTH_LONG).show()
            }
        }
    }
}
