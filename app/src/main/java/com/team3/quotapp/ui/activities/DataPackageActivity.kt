package com.team3.quotapp.ui.activities

import android.content.Intent
import android.database.Cursor
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.ContactsContract
import android.text.Editable
import android.text.TextWatcher
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.team3.quotapp.R
import com.team3.quotapp.rest.model.InetDataModel
import com.team3.quotapp.rest.response.InternetDataList
import com.team3.quotapp.ui.adapters.InternetDataAdapter
import com.team3.quotapp.ui.fragments.FragmentPaymentConfirmation
import com.team3.quotapp.viewmodel.QuotaViewModel
import kotlinx.android.synthetic.main.activity_data_package.*
import org.koin.android.ext.android.inject

class DataPackageActivity : AppCompatActivity() {
    private val quotaViewModel: QuotaViewModel by inject()
    private val adapter: InternetDataAdapter by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_data_package)
        setupToolbar()
        setupRecyclerView()
        itemOnClick()
        searchQuota()
        searchContact()
    }

    private fun setupToolbar() {
        setSupportActionBar(toolbar)
        assert(supportActionBar != null)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_keyboard_arrow_left_grey_24dp)
        supportActionBar?.title = ""
    }

    private fun setupRecyclerView() {
        recyler_view.layoutManager = LinearLayoutManager(this)
        recyler_view.setHasFixedSize(true)
        recyler_view.adapter = adapter
    }

    private fun searchQuota() {
        etPhoneRegister.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if (etPhoneRegister.text.length >= 8) {
                    getAll(InetDataModel(nomerHp = "+62${etPhoneRegister.text}"))
                }
            }
        })
    }

    fun getAll(mobileNumber: InetDataModel) {
        quotaViewModel.getInternetDataList(mobileNumber)
            .observe(this, Observer<List<InternetDataList>> { list ->
                list?.let {
                    adapter.setInternetData(it.sortedBy { it.harga })
                }
            })
    }

    private fun searchContact() {
        contact.setOnClickListener {
            let {
                val intent = Intent(Intent.ACTION_GET_CONTENT)
                intent.type = ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE
                it.startActivityForResult(intent, 1)
            }
        }
    }

    private fun itemOnClick() {
        adapter.setOnItemClickListener(object : InternetDataAdapter.OnItemClickListener {
            override fun onItemClick(inetData: InternetDataList) {
                val fragment = FragmentPaymentConfirmation()
                val bundle = Bundle()
                bundle.putParcelable("value", inetData)
                bundle.putString("mobileNumber", "+62${etPhoneRegister.text}")
                fragment.arguments = bundle
                fragment.show(supportFragmentManager, "exampleBottomSheet")
            }
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 1) {
            if (resultCode == AppCompatActivity.RESULT_OK) {
                val contactData: Uri? = data?.data
                val cursor: Cursor = managedQuery(contactData, null, null, null, null)
                cursor.moveToFirst()
                var number =
                    cursor.getString(cursor.getColumnIndexOrThrow(ContactsContract.CommonDataKinds.Phone.NUMBER))
                number = number.replace("\\s".toRegex(), "")
                number = number.replace("^(\\+62)".toRegex(), "")
                etPhoneRegister.setText(number)
            }
        }
    }
}
