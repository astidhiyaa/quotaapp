package com.team3.quotapp.ui.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.team3.quotapp.R
import androidx.lifecycle.Observer
import com.team3.quotapp.preference.PreferenceManager
import com.team3.quotapp.rest.model.ChangePasswordRequest
import com.team3.quotapp.rest.response.ChangePasswordResponse
import com.team3.quotapp.ui.fragments.CustomProgressDialog
import com.team3.quotapp.viewmodel.QuotaViewModel
import kotlinx.android.synthetic.main.activity_change_password_email.*
import org.koin.android.ext.android.inject

class ChangePasswordEmailActivity : AppCompatActivity() {
    private val quotaViewModel: QuotaViewModel by inject()
    private lateinit var preferenceManager: PreferenceManager
    private val progressDialog = CustomProgressDialog()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_change_password_email)
        setupToolbar()
        onClick()
    }

    private fun setupToolbar() {
        setSupportActionBar(toolbar)
        assert(supportActionBar != null)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_keyboard_arrow_left_grey_24dp)
        supportActionBar?.title = ""
        toolbar.setNavigationOnClickListener { view: View? -> onBackPressed() }
    }

    private fun onClick() {
        btn_change_password.setOnClickListener {
            progressDialog.show(this,"Please Wait...")
            initData()
        }
    }

    private fun initData() {
        val email = et_email.text.toString()
        val password = et_password.text.toString()
        val confirmPassword = et_confirm_password.text.toString()

        confirmForgotPassword(email, password, confirmPassword)
    }

    private fun setLogIn(data: ChangePasswordResponse) {
        preferenceManager = PreferenceManager(this)
        preferenceManager.setChangePassword(data)
    }

    private fun confirmForgotPassword(email: String, newPassword: String, confirmPassword: String) {
        quotaViewModel.changePassword(
            ChangePasswordRequest(email, newPassword, confirmPassword))
            .observe(this, Observer<ChangePasswordResponse> {
                if (it?.status.equals("200")) {
                    progressDialog.dialog.dismiss()
                    setLogIn(it)
                    preferenceManager.setLoggedIn(true)
                    val preference = getSharedPreferences("QuotaApp", 0)
                    val editor = preference.edit()
                    editor.putString("PREF_PASSWORD", et_password.text.toString().trim())
                    editor.apply()
                    startActivity(Intent(this, MainActivity::class.java))
                } else {
                    progressDialog.dialog.dismiss()
                    Toast.makeText(applicationContext, "Something went wrong! ${it.message}",
                    Toast.LENGTH_LONG).show()
                }
            })
    }
}
