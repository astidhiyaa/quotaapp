package com.team3.quotapp.ui.fragments

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.material.bottomsheet.BottomSheetDialogFragment

import com.team3.quotapp.R
import com.team3.quotapp.rest.model.UserModel
import com.team3.quotapp.ui.activities.LoginActivity
import com.team3.quotapp.ui.activities.OtpActivity
import kotlinx.android.synthetic.main.fragment_verify_register.*

class FragmentVerifyRegister : BottomSheetDialogFragment() {

    override fun getTheme(): Int = R.style.BottomSheetDialogTheme

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val viewRoot = inflater.inflate(R.layout.fragment_verify_register, container, false)
        return viewRoot
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btn_choose_email.setOnClickListener {
            tvOkEmail.visibility = View.VISIBLE
            btn_choose_phone.text = "Go to login page"
            btn_choose_phone.setOnClickListener {
                activity?.let{
                    val intent = Intent (it, LoginActivity::class.java)
                    it.startActivity(intent)
                }
            }
        }
        btn_choose_phone.setOnClickListener {
            if(arguments != null) {
                var mobileNumber = arguments!!.getString("mobileNumber")
                var userInfo = arguments!!.getParcelable<UserModel>("value")
                activity?.let {
                    val intent = Intent(it, OtpActivity::class.java)
                    intent.putExtra("mobileNumber",mobileNumber)
                    intent.putExtra("value", userInfo)
                    it.startActivity(intent)
                }
            }
        }
    }
}
