package com.team3.quotapp.ui.adapters

import android.content.Context
import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.team3.quotapp.R
import com.team3.quotapp.rest.response.HistoryList

class SuccessHistoryAdapter : RecyclerView.Adapter<SuccessHistoryAdapter.HistoryHolder>() {
    private var historyEntityList: List<HistoryList> = ArrayList()
    private var mContext: Context? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HistoryHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_history, parent, false)
        mContext = parent.context
        return HistoryHolder(itemView)
    }

    override fun onBindViewHolder(holder: HistoryHolder, position: Int) {
        val currentHistory = historyEntityList[position]
        holder.nameTextView.text = currentHistory.nomorPaketData
        holder.dateTextView.text = customDate(currentHistory.tanggal)
        holder.priceTextView.text = customPrice(currentHistory.harga.toString())
    }

    override fun getItemCount(): Int {
        return historyEntityList.size
    }

    fun setHistory(pulseData: List<HistoryList>) {
        this.historyEntityList = pulseData
        notifyDataSetChanged()
    }

    inner class HistoryHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var operatorImageView: ImageView = itemView.findViewById(R.id.iv_operator)
        var nameTextView: TextView = itemView.findViewById(R.id.tv_package_name)
        var dateTextView: TextView = itemView.findViewById(R.id.tv_transaction_date)
        var priceTextView: TextView = itemView.findViewById(R.id.tv_package_price)
    }

    private fun customDate(date: String): String {
        val text = date.take(10)
        return text
    }

    private fun customPrice(price: String): String {
        val text = price.replace(".0","")
        val text2 = "Rp ${text}"
        return text2
    }
}
