package com.team3.quotapp.ui.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.team3.quotapp.R
import com.team3.quotapp.ui.adapters.MyPagerAdapter
import kotlinx.android.synthetic.main.fragment_history.*

class HistoryFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.fragment_history, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mainViewPager.adapter = MyPagerAdapter(childFragmentManager)
        mainTabLayout.setupWithViewPager(mainViewPager)
        mainTabLayout.getTabAt(0)!!
        mainTabLayout.getTabAt(1)!!
    }
}
