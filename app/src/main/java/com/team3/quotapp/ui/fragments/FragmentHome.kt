package com.team3.quotapp.ui.fragments

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.team3.quotapp.R
import com.team3.quotapp.preference.PreferenceManager
import com.team3.quotapp.ui.activities.DataPackageActivity
import kotlinx.android.synthetic.main.fragment_home.*
import java.text.NumberFormat
import java.util.*

class FragmentHome : Fragment() {
    private lateinit var preferenceManager: PreferenceManager

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        onClickGroup()
    }

    private fun initView() {
        preferenceManager = PreferenceManager(activity!!.applicationContext)

        if (preferenceManager.getUserData().saldo.equals("")) {
            val saldo = 0
            val localeID = Locale("in", "ID")
            val numberFormat = NumberFormat.getCurrencyInstance(localeID)
            tv_saldo.text = numberFormat.format(saldo.toDouble()).toString()
        } else {
            val saldo = preferenceManager.getUserData().saldo
            val localeID = Locale("in", "ID")
            val numberFormat = NumberFormat.getCurrencyInstance(localeID)
            tv_saldo.text = numberFormat.format(saldo.toDouble()).toString()
        }

        val saldo = preferenceManager.getUserData().saldo
        val localeID =  Locale("in", "ID")
        val numberFormat = NumberFormat.getCurrencyInstance(localeID)
        tv_saldo.text = numberFormat.format(saldo!!.toDouble()).toString()

    }

    private fun onClickGroup() {
        cl_data_package.setOnClickListener {
            val intent = Intent(activity, DataPackageActivity::class.java)
            startActivity(intent)
        }
    }
}
