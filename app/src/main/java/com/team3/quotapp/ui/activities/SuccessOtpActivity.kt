package com.team3.quotapp.ui.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.team3.quotapp.R
import kotlinx.android.synthetic.main.activity_success_otp.*

class SuccessOtpActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_success_otp)
        onClick()
    }

    private fun onClick() {
        btn_start_using.setOnClickListener {
            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
        }
    }
}
