package com.team3.quotapp.ui.fragments

import android.content.ContentProvider
import android.content.Intent
import android.database.Cursor
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.team3.quotapp.R
import com.team3.quotapp.data.db.HistoryEntity
import com.team3.quotapp.preference.PreferenceManager
import com.team3.quotapp.rest.model.TransactionChosenModel
import com.team3.quotapp.rest.model.VAModel
import com.team3.quotapp.rest.model.WalletModel
import com.team3.quotapp.rest.response.InternetDataList
import com.team3.quotapp.rest.response.PayNowResponse
import com.team3.quotapp.rest.response.SignUpResponse
import com.team3.quotapp.rest.response.TransactionResponseChosen
import com.team3.quotapp.viewmodel.HistoryViewModel
import com.team3.quotapp.viewmodel.QuotaViewModel
import kotlinx.android.synthetic.main.dialog_payment_confirmation.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.asRequestBody
import org.koin.android.ext.android.inject
import java.io.File
import java.text.NumberFormat
import java.text.SimpleDateFormat
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*

class FragmentPaymentConfirmation : BottomSheetDialogFragment() {
    private lateinit var preferenceManager: PreferenceManager
    var filePath: Uri? = null
    private val quotaViewModel: QuotaViewModel by inject()
    private val historyViewModel: HistoryViewModel by inject()
    private val progressDialog = CustomProgressDialog()

    override fun getTheme(): Int = R.style.BottomSheetDialogTheme

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val viewRoot = inflater.inflate(R.layout.dialog_payment_confirmation, container, false)
        return viewRoot
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (arguments != null){
            val sent = arguments!!.getParcelable<InternetDataList>("value")
            val mobileNumber = arguments!!.getString("mobileNumber")
            preferenceManager = PreferenceManager(activity!!.applicationContext)
            val saldo = preferenceManager.getUserData().saldo
            val localeID =  Locale("in", "ID")
            val numberFormat = NumberFormat.getCurrencyInstance(localeID)
            val saldoRp = numberFormat.format(saldo.toDouble()).toString()
            val preferences = activity?.getSharedPreferences("QuotaApp",0)
            val phone = preferences?.getString("PREF_PHONE","")
            initView(sent!!)

            btn_choose_method.setOnClickListener {
                radio_payment_method.visibility = View.VISIBLE
                rb_wallet.setOnClickListener {
                    if (rb_wallet.isChecked) {
                        walletVisibility(saldoRp)
                        btn_choose_method.setOnClickListener {
                            quotaViewModel.chooseInetPackage(
                                TransactionChosenModel(
                                    namaProvider = sent.namaProvider,
                                    harga = sent.harga!!.toInt(),
                                    paketData = sent!!.paketData,
                                    noTelepon = phone,
                                    nomorPaketData = mobileNumber
                                )
                            )
                                .observe(this, Observer<TransactionResponseChosen> {
                                    if (et_pin.text!!.length < 6) btn_choose_method.isEnabled = false
                                    if (it.id != null) payWithWallet(
                                        phone!!, et_pin.text.toString().toInt(), saldo,
                                        sent!!.harga, sent!!.paketData, sent!!.namaProvider, it!!.waktuTransaksi
                                    )
                                })
                        }
                    }
                }

                rb_va.setOnClickListener {
                    if (rb_va.isChecked) {
                        VAVisibility(phone!!)
                        btn_choose_method.setOnClickListener {
                            quotaViewModel.chooseInetPackage(
                                TransactionChosenModel(
                                    namaProvider = sent.namaProvider,
                                    harga = sent.harga!!.toInt(),
                                    paketData = sent!!.paketData,
                                    noTelepon = phone,
                                    nomorPaketData = mobileNumber
                                )
                            )
                                .observe(this, Observer<TransactionResponseChosen> {
                                    val phoneNumber = phone.replace("+62", "0")
                                    val virtualAccount = "8000$phoneNumber".toLong()
                                    if (it.id != null) payWithVA(phone, virtualAccount, sent!!.harga, sent!!.paketData, sent!!.namaProvider, it!!.waktuTransaksi)
                                })
                        }
                    }
                }
            }

            btn_cancel.setOnClickListener {
                dismiss()
            }
        }
    }
    private fun walletVisibility(saldoRp: String){
        layout_bni.visibility = View.GONE
        layout_mandiri.visibility = View.GONE
        layout_bri.visibility = View.GONE
        tvDialogWalletBalance.visibility = View.VISIBLE
        tv_pin.visibility = View.VISIBLE
        layout_pin.visibility = View.VISIBLE
        et_pin.visibility = View.VISIBLE
        tvDialogWalletBalance.text = "Your balance is $saldoRp"
        btn_choose_method.text = "Pay Now"
    }

    private fun VAVisibility(phone: String){
        tvDialogWalletBalance.visibility = View.GONE
        tv_pin.visibility = View.GONE
        layout_pin.visibility = View.GONE
        layout_pin.visibility = View.GONE
        et_pin.visibility = View.GONE
        layout_mandiri.visibility = View.VISIBLE
        layout_bri.visibility = View.VISIBLE
        tvBankBni.text = "Virtual Account Number: 8000${phone!!.replace("+62","0")}"
        tvBankMandiri.text = "Virtual Account Number: 8000${phone!!.replace("+62","0")}"
        tvBankBri.text = "Virtual Account Number: 8000${phone.replace("+62","0")}"
        btn_choose_method.text = "Pay Now"
    }

    fun payWithWallet(mobileNumber:String, pinTransaksi: Int, saldo: String, harga: String?, paketData: String?, namaProvider: String?, currentDate: String?){
        progressDialog.show(context!!,"Please Wait...")
        quotaViewModel.setWallet(WalletModel(noTelepon = mobileNumber,pinTransaksi = pinTransaksi))
            .observe(this, Observer<PayNowResponse>{
                if(it.status == "200"){
                    progressDialog.dialog.dismiss()
                    val preference = activity?.getSharedPreferences("QuotaApp", 0)
                    val editor = preference?.edit()
                    val newSaldo = saldo.toLong().minus(harga!!.toLong())
                    editor?.putString("PREF_SALDO", newSaldo.toString())
                    editor?.apply()
                    Toast.makeText(activity, "Pembayaran berhasil", Toast.LENGTH_LONG).show()
                    dismiss()
                } else {
                    progressDialog.dialog.dismiss()
                    Toast.makeText(
                        activity, "Something went wrong! ${it.message}", Toast.LENGTH_LONG)
                        .show()
                    val username = preferenceManager.getUserData().username
                    val phone = mobileNumber
                    val provider = namaProvider!!
                    val name = paketData!!
                    val price = harga!!.toInt()
                    val date = currentDate!!
                    val history = HistoryEntity(username, phone, provider, name, price ,date)
                    historyViewModel.insertHistory(history)

                    val preference = activity?.getSharedPreferences("QuotaApp", 0)
                    val editor = preference?.edit()
                    editor?.putString("PREF_LAST_PROVIDER", phone)
                    editor?.apply()
                }
            })
    }

    private fun payWithVA(mobileNumber:String, virtualAccount: Long, harga: String?, paketData: String?, namaProvider: String?, currentDate: String?){
        progressDialog.show(context!!,"Please Wait...")
        quotaViewModel.setVA(VAModel(noTelepon = mobileNumber,virtualAccount = virtualAccount))
            .observe(this, Observer<PayNowResponse>{
                if (it.status == "200") {
                    progressDialog.dialog.dismiss()
                    btn_choose_method.text = "Choose Image"
                    btn_choose_method.setOnClickListener {
                        val cameraIntent = Intent(
                            Intent.ACTION_PICK,
                            MediaStore.Images.Media.EXTERNAL_CONTENT_URI
                        )
                        btn_cancel.text = "Upload VA"
                        btn_cancel.backgroundTintList = (this.resources.getColorStateList(R.color.blue))
                        startActivityForResult(cameraIntent,1)
                    }
                    btn_cancel.setOnClickListener {
                        progressDialog.show(context!!,"Please Wait...")
                        val file = File(getRealPathFromURI(filePath))
                        val body = MultipartBody.Part.createFormData("file",file.name, file.asRequestBody("image/*".toMediaTypeOrNull()))
                        if(filePath != null){
                            quotaViewModel.uploadImgVA(body).observe(this, Observer<SignUpResponse>{
                                if(it.status == "200"){
                                    progressDialog.dialog.dismiss()
                                    Toast.makeText(activity, "Pembayaran berhasil!",Toast.LENGTH_LONG).show()
                                    dismiss()
                                }
                                else{
                                    progressDialog.dialog.dismiss()
                                    Toast.makeText(activity, "Something went wrong! ${it.message}",Toast.LENGTH_LONG).show()
                                }
                            })
                        }
                        else{
                            btn_cancel.isEnabled = false
                            progressDialog.dialog.dismiss()
                            Toast.makeText(activity, "Please, select image!",Toast.LENGTH_LONG).show()
                        }
                    }
                } else {
                    progressDialog.dialog.dismiss()
                    Toast.makeText(activity, "Something went wrong! ${it.status}",
                        Toast.LENGTH_LONG).show()
                    val username = preferenceManager.getUserData().username
                    val phone = mobileNumber
                    val provider = namaProvider!!
                    val name = paketData!!
                    val price = harga!!.toInt()
                    val date = currentDate!!
                    val history = HistoryEntity(username, phone, provider, name, price ,date)
                    historyViewModel.insertHistory(history)

                    val preference = activity?.getSharedPreferences("QuotaApp", 0)
                    val editor = preference?.edit()
                    editor?.putString("PREF_LAST_PROVIDER", phone)
                    editor?.apply()
                }
            })
    }

    private fun initView(sent : InternetDataList){
        if (sent.namaProvider.equals("Simpati",ignoreCase = true) || sent.namaProvider.equals("AS",ignoreCase = true)) iv_operator.setImageResource(R.drawable.logo_telkomsel)
        else if (sent.namaProvider.equals("Mentari",ignoreCase = true) || sent.namaProvider.equals("IM3")) iv_operator.setImageResource(R.drawable.logo_indosat)
        else if (sent.namaProvider.equals("XL",ignoreCase = true)) iv_operator.setImageResource(R.drawable.logo_xl)
        else if (sent.namaProvider.equals("Three",ignoreCase = true)) iv_operator.setImageResource(R.drawable.logo_three)
        else if (sent.namaProvider.equals("Smartfren",ignoreCase = true)) iv_operator.setImageResource(R.drawable.logo_smartfren)
        else if (sent.namaProvider.equals("AXIS",ignoreCase = true)) iv_operator.setImageResource(R.drawable.logo_axis)
        val localeID =  Locale("in", "ID")
        val numberFormat = NumberFormat.getCurrencyInstance(localeID)
        val hargaRp = numberFormat.format(sent.harga!!.toDouble()).toString()
        tv_confirmation2.text = hargaRp
        tv_confirmation_desc.text = "Your package is ${sent?.paketData} and the package price is $hargaRp. Select the payment method to continue purchasing"
    }

    private fun getRealPathFromURI(contentUri: Uri?): String? {
        val proj = arrayOf(MediaStore.Images.Media.DATA)
        val cursor: Cursor = activity!!.managedQuery(
            contentUri,
            proj,
            null,
            null,
            null
        )
        val column_index: Int = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
        cursor.moveToFirst()
        return cursor.getString(column_index)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode === 1) {
            if(resultCode == AppCompatActivity.RESULT_OK) {
                filePath = data?.data
            }
        }
    }
}