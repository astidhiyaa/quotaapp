package com.team3.quotapp.ui.activities

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.team3.quotapp.R
import com.team3.quotapp.preference.PreferenceManager
import com.team3.quotapp.rest.model.InOutRequest
import com.team3.quotapp.rest.response.InOutResponse
import com.team3.quotapp.ui.fragments.CustomProgressDialog
import com.team3.quotapp.viewmodel.QuotaViewModel
import kotlinx.android.synthetic.main.activity_login.*
import org.koin.android.ext.android.inject

class LoginActivity : AppCompatActivity() {
    private val quotaViewModel: QuotaViewModel by inject()
    private lateinit var preferenceManager: PreferenceManager
    private val progressDialog = CustomProgressDialog()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        setupToolbar()
        onClickGroup()
    }

    private fun setupToolbar() {
        setSupportActionBar(toolbar)
        assert(supportActionBar != null)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_keyboard_arrow_left_grey_24dp)
        supportActionBar?.title = ""
    }

    private fun onClickGroup() {
        btn_login.setOnClickListener {
            progressDialog.show(this,"Please Wait...")
            initData()
        }

        tv_forgot.setOnClickListener {
            val intent = Intent(this, ForgotPasswordActivity::class.java)
            startActivity(intent)
        }
    }

    private fun initData() {
        val phone = et_phone.text.toString().trim()
        val phoneNumber = "+62${phone}"
        val password = et_password.text.toString()

        login(phoneNumber, password)
    }

    private fun setLogIn(data: InOutResponse) {
        preferenceManager = PreferenceManager(this)
        preferenceManager.setLoggedIn(true)
        preferenceManager.setUserData(data)
    }

    private fun login(phone: String, pass: String) {
        quotaViewModel.login(InOutRequest(phone, pass)).observe(this, Observer<InOutResponse> {
            if (it?.status == "200") {
                progressDialog.dialog.dismiss()
                setLogIn(it)
                val preference = getSharedPreferences("QuotaApp", 0)
                val editor = preference.edit()
                editor.putString("PREF_PHONE", "+62" + et_phone.text.toString().trim())
                editor.putString("PREF_PASSWORD", et_password.text.toString().trim())
                editor.putString("PREF_SALDO",it.saldo)
                Log.d("Success", it.token)
                editor.apply()
                startActivity(Intent(this, MainActivity::class.java))
            }
            else {
                progressDialog.dialog.dismiss()
                Toast.makeText(
                    this, "Something went wrong! ${it.message}", Toast.LENGTH_LONG)
                    .show()
            }
        })
    }
}
