package com.team3.quotapp.ui.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import com.team3.quotapp.R
import com.team3.quotapp.preference.PreferenceManager
import com.team3.quotapp.rest.model.EditUserRequest
import com.team3.quotapp.rest.model.InOutRequest
import com.team3.quotapp.rest.response.EditUserResponse
import com.team3.quotapp.rest.response.InOutResponse
import com.team3.quotapp.ui.fragments.CustomProgressDialog
import com.team3.quotapp.viewmodel.QuotaViewModel
import kotlinx.android.synthetic.main.activity_change_user.*
import org.koin.android.ext.android.inject

class ChangeUserActivity : AppCompatActivity() {
    private val quotaViewModel: QuotaViewModel by inject()
    private lateinit var preferenceManager: PreferenceManager
    private val progressDialog = CustomProgressDialog()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_change_user)
        setupToolbar()
        onClick()
    }

    private fun setupToolbar() {
        setSupportActionBar(toolbar)
        assert(supportActionBar != null)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_keyboard_arrow_left_grey_24dp)
        supportActionBar?.title = ""
        toolbar.setNavigationOnClickListener { view: View? -> onBackPressed() }
    }

    private fun onClick() {
        btn_continue.setOnClickListener {
            progressDialog.show(this,"Please Wait...")
            initEditUser()
        }
    }

    private fun initEditUser() {
        val phone = "+62${et_phone.text.toString()}"
        val username = et_full_name.text.toString()
        val email = et_email.text.toString()
        editUser(phone, username, email)
    }

    private fun editUser(phone: String, username: String, email: String) {
        preferenceManager = PreferenceManager(applicationContext)
        quotaViewModel.editUser(phone, EditUserRequest(username, phone, email)).observe(this, Observer<EditUserResponse> {
            if (it.status.equals("200")) {
                progressDialog.dialog.dismiss()
                initLogOut()
                preferenceManager.setEditUser(it)
            } else {
                progressDialog.dialog.dismiss()
                Toast.makeText(this, "Something went wrong! ${it.message}", Toast.LENGTH_LONG).show()
            }
        })
    }

    private fun initLogOut() {
        val preference = getSharedPreferences("QuotaApp", 0)
        val phone = preference.getString("PREF_PHONE", "").toString()
        val password = preference.getString("PREF_PASSWORD", "").toString()
        logout(phone, password)
    }

    private fun logout(phone: String, pass: String) {
        quotaViewModel.logout(InOutRequest(phone, pass)).observe(this, Observer<InOutResponse> {
            if (it.status == "200") {
                preferenceManager.setLoggedIn(false)
                val intent = Intent(this, LoginActivity::class.java)
                startActivity(intent)
            } else {
                Toast.makeText(
                    this, "Something went wrong! ${it.message}", Toast.LENGTH_LONG
                ).show()
            }
        })
    }
}
