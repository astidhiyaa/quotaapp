package com.team3.quotapp.ui.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.lifecycle.Observer
import com.team3.quotapp.R
import com.team3.quotapp.rest.model.ForgotPasswordRequest
import com.team3.quotapp.rest.response.ForgotPasswordResponse
import com.team3.quotapp.ui.fragments.ChangeMediaFragment
import com.team3.quotapp.viewmodel.QuotaViewModel
import kotlinx.android.synthetic.main.activity_forgot_password.*
import org.koin.android.ext.android.inject

class ForgotPasswordActivity : AppCompatActivity() {
    private val quotaViewModel: QuotaViewModel by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_forgot_password)
        setupToolbar()
        onClickGroup()
    }

    private fun setupToolbar() {
        setSupportActionBar(toolbar)
        assert(supportActionBar != null)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_keyboard_arrow_left_grey_24dp)
        supportActionBar?.title = ""
    }

    private fun onClickGroup() {
        btn_continue.setOnClickListener {
            initData()
        }
    }

    private fun initData() {
        val email = et_email.text.toString().trim()
        val phone = et_phone.text.toString().trim()
        val phoneNumber = "+62${phone}"

        val preference = getSharedPreferences("QuotaApp", 0)
        val editor = preference.edit()
        editor.putString("PREF_PHONE", phoneNumber)
        editor.putString("PREF_EMAIL", email)
        editor.apply()

        forgotPassword(email, phoneNumber)
    }

    private fun forgotPassword(email: String, phone: String) {
        quotaViewModel.forgotPassword(ForgotPasswordRequest(email, phone))
            .observe(this, Observer<ForgotPasswordResponse> {
                if (it?.status.equals("200")) {
                    val bottomFragment = ChangeMediaFragment()
                    val bundle = Bundle()
                    bottomFragment.arguments = bundle
                    bottomFragment.show(supportFragmentManager, "bottomSheet")
                } else {
                    Toast.makeText(
                        applicationContext, "Something went wrong! ${it.message}",
                        Toast.LENGTH_LONG
                    ).show()
                }
            })
    }
}
