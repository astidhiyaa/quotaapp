package com.team3.quotapp.ui.adapters

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.team3.quotapp.R
import com.team3.quotapp.rest.response.InternetDataList
import java.text.NumberFormat
import java.util.*
import kotlin.collections.ArrayList


class InternetDataAdapter: RecyclerView.Adapter<InternetDataAdapter.InternetDataHolder>() {
    private var internetDataList: List<InternetDataList> = ArrayList()
    private var listener: OnItemClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): InternetDataHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.item_quota,parent,false)
        return InternetDataHolder(itemView)
    }

    override fun onBindViewHolder(holder: InternetDataAdapter.InternetDataHolder, position: Int) {
        val current: InternetDataList = internetDataList[position]

        holder.tvPackage.text = current.paketData
        holder.tvPrice.text = convertToRp(current.harga!!)
        holder.tvProvider.text = current.namaProvider
    }

    override fun getItemCount(): Int {
        return internetDataList.size
    }

    fun setInternetData(internetData: List<InternetDataList>){
        this.internetDataList = internetData
        notifyDataSetChanged()
    }

    inner class InternetDataHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        init {
            itemView.setOnClickListener {
                val position = adapterPosition
                if (position != RecyclerView.NO_POSITION) {
                    listener?.onItemClick((internetDataList[position]))
                }
            }
        }
        var tvPackage: TextView = itemView.findViewById(R.id.tvPackage)
        var tvPrice: TextView = itemView.findViewById(R.id.tvPrice)
        var tvProvider: TextView = itemView.findViewById(R.id.tvProvider)
    }
    interface OnItemClickListener {
        fun onItemClick(internetDataList: InternetDataList)
    }

    fun setOnItemClickListener(listener: OnItemClickListener) {
        this.listener = listener
    }

    fun convertToRp(harga: String): String{
        val localeID =  Locale("in", "ID")
        val numberFormat = NumberFormat.getCurrencyInstance(localeID)
        return numberFormat.format(harga!!.toDouble()).toString()
    }
}
