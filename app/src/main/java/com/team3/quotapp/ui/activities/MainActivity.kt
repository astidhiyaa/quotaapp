package com.team3.quotapp.ui.activities

import android.content.Intent
import android.content.pm.PackageManager
import android.database.Cursor
import android.net.Uri
import android.os.Bundle
import android.provider.ContactsContract
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.team3.quotapp.R
import com.team3.quotapp.ui.fragments.FragmentHome
import com.team3.quotapp.ui.fragments.FragmentProfile
import com.team3.quotapp.ui.fragments.HistoryFragment
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        checkPermissions()
        bottom_navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        val fragment = FragmentHome()
        addFragment(fragment)
    }

    fun checkPermissions() {
        if (ContextCompat.checkSelfPermission(
                this,
                android.Manifest.permission.READ_CONTACTS
            ) != PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(
                this,
                android.Manifest.permission.READ_EXTERNAL_STORAGE
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(
                    android.Manifest.permission.READ_CONTACTS,
                    android.Manifest.permission.READ_EXTERNAL_STORAGE
                ), 1
            )
        }
    }

    private val mOnNavigationItemSelectedListener =
        BottomNavigationView.OnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.page_1 -> {
                    val home = FragmentHome()
                    addFragment(home)
                    return@OnNavigationItemSelectedListener true
                }
                R.id.page_2 -> {
                    val history = HistoryFragment()
                    addFragment(history)
                    return@OnNavigationItemSelectedListener true
                }
                R.id.page_3 -> {
                    val profile = FragmentProfile()
                    addFragment(profile)
                    return@OnNavigationItemSelectedListener true
                }
            }
            false
        }

    private fun addFragment(fragment: Fragment) {
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.fl_container, fragment, fragment.javaClass.simpleName)
            .commit()
    }
    
    override fun onBackPressed() {
        super.onBackPressed()
        this.finishAffinity()
        finishAndRemoveTask()
    }
}