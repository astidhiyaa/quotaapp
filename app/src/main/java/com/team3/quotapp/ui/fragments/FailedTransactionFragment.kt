package com.team3.quotapp.ui.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager

import com.team3.quotapp.R
import com.team3.quotapp.ui.adapters.FailedHistoryAdapter
import com.team3.quotapp.viewmodel.HistoryViewModel
import kotlinx.android.synthetic.main.fragment_failed_transaction.*
import org.koin.android.ext.android.inject

class FailedTransactionFragment : Fragment() {
    private val historyAdapter: FailedHistoryAdapter by inject()
    private val historyViewModel: HistoryViewModel by inject()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_failed_transaction, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
    }

    private fun initView() {
        historyViewModel.getAllHistory().observe(this, Observer { list -> list.let { historyAdapter.setHistory(it) } })
        rv_history.layoutManager = LinearLayoutManager(context)
        rv_history.setHasFixedSize(true)
        rv_history.addItemDecoration(DividerItemDecoration(rv_history.context, LinearLayoutManager.VERTICAL))
        rv_history.adapter = historyAdapter
    }
}
