package com.team3.quotapp.ui.fragments

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.material.bottomsheet.BottomSheetDialogFragment

import com.team3.quotapp.R
import com.team3.quotapp.ui.activities.ChangePasswordEmailActivity
import com.team3.quotapp.ui.activities.ChangePasswordOtpActivity
import kotlinx.android.synthetic.main.fragment_change_media.*

class ChangeMediaFragment : BottomSheetDialogFragment() {

    override fun getTheme(): Int = R.style.BottomSheetDialogTheme

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.fragment_change_media, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        onClickGroup()
    }

    private fun onClickGroup(){
        verify_email.setOnClickListener {
            val intent = Intent(activity, ChangePasswordEmailActivity::class.java)
            startActivity(intent)
        }

        verify_phone.setOnClickListener {
            val intent = Intent(activity, ChangePasswordOtpActivity::class.java)
            startActivity(intent)
        }
    }
}
