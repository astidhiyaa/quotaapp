package com.team3.quotapp.preference

import android.content.Context
import android.content.SharedPreferences
import com.team3.quotapp.rest.response.ChangePasswordResponse
import com.team3.quotapp.rest.response.ConfirmForgotResponse
import com.team3.quotapp.rest.response.EditUserResponse
import com.team3.quotapp.rest.response.InOutResponse

class PreferenceManager(var context: Context) {
    private val PREFS_NAME = "QuotaApp"
    private val IS_LOGGED_IN = "isLoggedIn"
    val sharedPref: SharedPreferences =
        context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)

    fun setLoggedIn(isFirstTime: Boolean) {
        val editor: SharedPreferences.Editor = sharedPref.edit()
        editor.putBoolean(IS_LOGGED_IN, isFirstTime)
        editor.apply()
    }

    fun isLoggedIn(): Boolean {
        return sharedPref.getBoolean(IS_LOGGED_IN, false)
    }

    fun setUserData(data: InOutResponse) {
        val editor: SharedPreferences.Editor = sharedPref.edit()
        editor.putString("PREF_TOKEN", data.token)
        editor.putString("PREF_TYPE", data.type)
        editor.putInt("PREF_ID", data.id)
        editor.putString("PREF_NAME", data.username)
        editor.putString("PREF_EMAIL", data.email)
        editor.putString("PREF_SALDO", data.saldo)
        editor.putString("PREF_MESSAGE", data.message)
        editor.putString("PREF_STATUS", data.status)
        editor.apply()
    }

    fun getUserData(): InOutResponse {
        return InOutResponse(
            sharedPref.getString("PREF_TOKEN", "")!!,
            sharedPref.getString("PREF_TYPE", "")!!,
            sharedPref.getInt("PREF_ID", 0),
            sharedPref.getString("PREF_NAME", "")!!,
            sharedPref.getString("PREF_EMAIL", "")!!,
            sharedPref.getString("PREF_SALDO","")!!,
            sharedPref.getString("PREF_MESSAGE","")!!,
            sharedPref.getString("PREF_STATUS","")!!
        )
    }

    fun setConfirmForgot(data: ConfirmForgotResponse) {
        val editor: SharedPreferences.Editor = sharedPref.edit()
        editor.putInt("PREF_ID", data.id)
        editor.putString("PREF_PHONE", data.noTelepon)
        editor.putString("PREF_NAME", data.namaUser)
        editor.putString("PREF_EMAIL", data.email)
        editor.putString("PREF_PASSWORD", data.password)
        editor.putString("PREF_VA", data.virtualAccount)
        editor.putString("PREF_SALDO", data.saldo)
        editor.apply()
    }

    fun setChangePassword(data: ChangePasswordResponse) {
        val editor: SharedPreferences.Editor = sharedPref.edit()
        editor.putInt("PREF_ID", data.id)
        editor.putString("PREF_PHONE", data.noTelepon)
        editor.putString("PREF_NAME", data.namaUser)
        editor.putString("PREF_EMAIL", data.email)
        editor.putString("PREF_PASSWORD", data.password)
        editor.putString("PREF_VA", data.virtualAccount)
        editor.putString("PREF_SALDO", data.saldo)
        editor.apply()
    }

    fun setEditUser(data: EditUserResponse) {
        val editor: SharedPreferences.Editor = sharedPref.edit()
        editor.putInt("PREF_ID", data.id)
        editor.putString("PREF_PHONE", data.noTelepon)
        editor.putString("PREF_NAME", data.namaUser)
        editor.putString("PREF_EMAIL", data.email)
        editor.putString("PREF_PASSWORD", data.password)
        editor.putString("PREF_VA", data.virtualAccount)
        editor.putString("PREF_SALDO", data.saldo)
        editor.apply()
    }

    fun clearSharedPreference() {
        val editor: SharedPreferences.Editor = sharedPref.edit()
        //sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        editor.clear()
        editor.apply()
    }

    fun removeValue(KEY_NAME: String) {
        val editor: SharedPreferences.Editor = sharedPref.edit()
        editor.remove(KEY_NAME)
        editor.apply()
    }
}