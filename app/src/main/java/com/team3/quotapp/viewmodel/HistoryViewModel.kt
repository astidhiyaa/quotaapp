package com.team3.quotapp.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.team3.quotapp.data.db.HistoryEntity
import com.team3.quotapp.repositories.HistoryRepository
import com.team3.quotapp.rest.ApiClient
import com.team3.quotapp.rest.response.HistoryList

class HistoryViewModel(private val historyRepository: HistoryRepository) : ViewModel() {
    fun insertHistory(history: HistoryEntity) {
        historyRepository.insertHistory(history)
    }

    fun getAllHistory(): LiveData<List<HistoryEntity>> {
        return historyRepository.getAllHistory()
    }

    fun getHistory(mobileNumber: String): LiveData<List<HistoryList>>{
        return historyRepository.getHistory(mobileNumber)
    }


}