package com.team3.quotapp.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.team3.quotapp.repositories.QuotaRepository
import com.team3.quotapp.rest.model.*
import com.team3.quotapp.rest.response.*
import okhttp3.MultipartBody

class QuotaViewModel(private var repository: QuotaRepository): ViewModel() {

    fun registerUser(userModel: UserModel): LiveData<SignUpResponse> {
        return repository.registerUser(userModel)
    }

    fun verifyPhoneNumber(mobileNumber: String, otpModel: OTPModel): LiveData<UserInfoResponse>{
        return repository.verifyPhoneNumber(mobileNumber,otpModel)
    }

    fun login(loginRequest: InOutRequest): LiveData<InOutResponse> {
        return repository.loginUser(loginRequest)
    }

    fun logout(logoutRequest: InOutRequest): LiveData<InOutResponse> {
        return repository.logoutUser(logoutRequest)
    }

    fun forgotPassword(forgotRequest: ForgotPasswordRequest): LiveData<ForgotPasswordResponse> {
        return repository.forgotPassword(forgotRequest)
    }

    fun confirmForgotPassword(mobile_number: String, confirmForgotRequest: ConfirmForgotRequest): LiveData<ConfirmForgotResponse> {
        return repository.confirmForgotPassword(mobile_number, confirmForgotRequest)
    }

    fun changePassword(changePasswordRequest: ChangePasswordRequest): LiveData<ChangePasswordResponse> {
        return repository.changePassword(changePasswordRequest)
    }

    fun resetPassword(resetRequest: ForgotPasswordRequest): LiveData<ForgotPasswordResponse> {
        return repository.resetPassword(resetRequest)
    }

    fun deleteAccount(phone: String): LiveData<DeleteAccountResponse> {
        return repository.deleteAccount(phone)
    }

    fun editUser(phone: String, editUserRequest: EditUserRequest): LiveData<EditUserResponse> {
        return repository.editUser(phone, editUserRequest)
    }

    fun getInternetDataList(mobileNumber: InetDataModel): LiveData<List<InternetDataList>>{
        return repository.getInternetDataList(mobileNumber)
    }

    fun getHistoryList(phone: String): LiveData<List<HistoryList>>{
        return repository.getHistoryList(phone)
    }

    fun chooseInetPackage(chosenModel: TransactionChosenModel):LiveData<TransactionResponseChosen>{
        return repository.chooseInetPackage(chosenModel)
    }

    fun setWallet(walletModel: WalletModel): LiveData<PayNowResponse>{
        return repository.setWallet(walletModel)
    }

    fun setVA(vaModel: VAModel): LiveData<PayNowResponse>{
        return repository.setVA(vaModel)
    }

    fun uploadImgVA (file: MultipartBody.Part) : LiveData<SignUpResponse>{
        return repository.uploadImageVA(file)
    }
}

