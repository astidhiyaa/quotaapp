package com.team3.quotapp.repositories

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.team3.quotapp.rest.ApiClient
import com.team3.quotapp.rest.model.*
import com.team3.quotapp.rest.response.*
import okhttp3.MultipartBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class QuotaRepository(private  var api: ApiClient) {

    fun registerUser(userModel: UserModel): MutableLiveData<SignUpResponse>{
        val data = MutableLiveData<SignUpResponse>()
        api.retrofit().registerUser(userModel)?.enqueue(object : Callback<SignUpResponse?>{
            override fun onResponse(call: Call<SignUpResponse?>, response: Response<SignUpResponse?>) {
                if (response.isSuccessful) {
                    data.value = response.body()
                } else {
                    val gson = Gson()
                    val type = object : TypeToken<SignUpResponse>() {}.type
                    val errorResponse: SignUpResponse? =
                        gson.fromJson(response.errorBody()!!.charStream(), type)
                    data.value = errorResponse
                }
            }
            override fun onFailure(call: Call<SignUpResponse?>, t: Throwable) {
                Log.d("Failed",t.toString())
            }
        })
        return data
    }

    fun verifyPhoneNumber(mobileNumber: String,otpModel: OTPModel): MutableLiveData<UserInfoResponse>{
        val data = MutableLiveData<UserInfoResponse>()
        api.retrofit().verifyPhoneNumber(mobileNumber,otpModel)?.enqueue(object : Callback<UserInfoResponse?>{
            override fun onResponse(call: Call<UserInfoResponse?>, response: Response<UserInfoResponse?>) {
                if(response.isSuccessful)
                    data.value = response.body()
                else {
                    val gson = Gson()
                    val type = object : TypeToken<UserInfoResponse>() {}.type
                    val errorResponse: UserInfoResponse? = gson.fromJson(response.errorBody()!!.charStream(), type)
                    data.value = errorResponse
                }
            }
            override fun onFailure(call: Call<UserInfoResponse?>, t: Throwable) {
                Log.d("Failed",t.toString())
            }
        })
        return data
    }

    fun loginUser(user: InOutRequest): MutableLiveData<InOutResponse> {
        val data = MutableLiveData<InOutResponse>()
        api.retrofit().login(user).enqueue(object : Callback<InOutResponse?> {
            override fun onResponse(call: Call<InOutResponse?>, response: Response<InOutResponse?>) {
                if (response.isSuccessful) {
                    data.value = response.body()
                    data.value?.status = response.code().toString()
                } else {
                    val gson = Gson()
                    val type = object : TypeToken<InOutResponse>() {}.type
                    val errorResponse: InOutResponse? =
                        gson.fromJson(response.errorBody()!!.charStream(), type)
                    data.value = errorResponse
                }
            }

            override fun onFailure(call: Call<InOutResponse?>, t: Throwable?) {
                Log.d("Login error", t.toString())
            }
        })
        return data
    }

    fun logoutUser(user: InOutRequest): MutableLiveData<InOutResponse> {
        val data = MutableLiveData<InOutResponse>()
        api.retrofit().logout(user).enqueue(object : Callback<InOutResponse?> {
            override fun onResponse(call: Call<InOutResponse?>, response: Response<InOutResponse?>) {
                if (response.isSuccessful) {
                    data.value = response.body()
                }
                else{
                    val gson = Gson()
                    val type = object : TypeToken<InOutResponse>() {}.type
                    val errorResponse: InOutResponse? = gson.fromJson(response.errorBody()!!.charStream(), type)
                    data.value = errorResponse
                }
            }

            override fun onFailure(call: Call<InOutResponse?>, t: Throwable?) {
                Log.d("Logout error", t.toString())
            }
        })
        return data
    }

    fun forgotPassword(change: ForgotPasswordRequest): MutableLiveData<ForgotPasswordResponse> {
        val data = MutableLiveData<ForgotPasswordResponse>()
        api.retrofit().forgotPassword(change).enqueue(object : Callback<ForgotPasswordResponse?> {
            override fun onResponse(call: Call<ForgotPasswordResponse?>, response: Response<ForgotPasswordResponse?>) {
                if (response.isSuccessful) {
                    data.value = response.body()
                }
                else{
                    val gson = Gson()
                    val type = object : TypeToken<ForgotPasswordResponse>() {}.type
                    val errorResponse: ForgotPasswordResponse? = gson.fromJson(response.errorBody()!!.charStream(), type)
                    data.value = errorResponse
                }
            }

            override fun onFailure(call: Call<ForgotPasswordResponse?>, t: Throwable?) {
                Log.d("Forgot password error", t.toString())
            }
        })
        return data
    }

    fun confirmForgotPassword(mobile_number: String, change: ConfirmForgotRequest): MutableLiveData<ConfirmForgotResponse> {
        val data = MutableLiveData<ConfirmForgotResponse>()
        api.retrofit().confirmForgotPassword(mobile_number, change).enqueue(object : Callback<ConfirmForgotResponse?> {
            override fun onResponse(call: Call<ConfirmForgotResponse?>, response: Response<ConfirmForgotResponse?>) {
                if (response.isSuccessful) {
                    data.value = response.body()
                }
                else{
                    val gson = Gson()
                    val type = object : TypeToken<ConfirmForgotResponse>() {}.type
                    val errorResponse: ConfirmForgotResponse? = gson.fromJson(response.errorBody()!!.charStream(), type)
                    data.value = errorResponse
                }
            }

            override fun onFailure(call: Call<ConfirmForgotResponse?>, t: Throwable?) {
                Log.d("Confirm forgot error", t.toString())
            }
        })
        return data
    }

    fun changePassword(change: ChangePasswordRequest): MutableLiveData<ChangePasswordResponse> {
        val data = MutableLiveData<ChangePasswordResponse>()
        api.retrofit().changePassword(change).enqueue(object : Callback<ChangePasswordResponse?> {
            override fun onResponse(call: Call<ChangePasswordResponse?>, response: Response<ChangePasswordResponse?>) {
                if (response.isSuccessful) {
                    data.value = response.body()
                }
                else{
                    val gson = Gson()
                    val type = object : TypeToken<ChangePasswordResponse>() {}.type
                    val errorResponse: ChangePasswordResponse? = gson.fromJson(response.errorBody()!!.charStream(), type)
                    data.value = errorResponse
                }
            }

            override fun onFailure(call: Call<ChangePasswordResponse?>, t: Throwable?) {
                Log.d("Change password error", t.toString())
            }
        })
        return data
    }

    fun resetPassword(change: ForgotPasswordRequest): MutableLiveData<ForgotPasswordResponse> {
        val data = MutableLiveData<ForgotPasswordResponse>()
        api.retrofit().resetPassword(change).enqueue(object : Callback<ForgotPasswordResponse?> {
            override fun onResponse(call: Call<ForgotPasswordResponse?>, response: Response<ForgotPasswordResponse?>) {
                if (response.isSuccessful) {
                    data.value = response.body()
                }
                else{
                    val gson = Gson()
                    val type = object : TypeToken<ForgotPasswordResponse>() {}.type
                    val errorResponse: ForgotPasswordResponse? = gson.fromJson(response.errorBody()!!.charStream(), type)
                    data.value = errorResponse
                }
            }

            override fun onFailure(call: Call<ForgotPasswordResponse?>, t: Throwable?) {
                Log.d("Reset password error", t.toString())
            }
        })
        return data
    }

    fun deleteAccount(phone: String): MutableLiveData<DeleteAccountResponse> {
        val data = MutableLiveData<DeleteAccountResponse>()
        api.retrofit().deleteAccount(phone).enqueue(object : Callback<DeleteAccountResponse?> {
            override fun onResponse(call: Call<DeleteAccountResponse?>, response: Response<DeleteAccountResponse?>) {
                if (response.isSuccessful) {
                    data.value = response.body()
                }
                else{
                    val gson = Gson()
                    val type = object : TypeToken<DeleteAccountResponse>() {}.type
                    val errorResponse: DeleteAccountResponse? = gson.fromJson(response.errorBody()!!.charStream(), type)
                    data.value = errorResponse
                }
            }

            override fun onFailure(call: Call<DeleteAccountResponse?>, t: Throwable?) {
                Log.d("Delete account error", t.toString())
            }
        })
        return data
    }

    fun editUser(phone: String, editUserRequest: EditUserRequest): MutableLiveData<EditUserResponse> {
        val data = MutableLiveData<EditUserResponse>()
        api.retrofit().editUser(phone, editUserRequest).enqueue(object : Callback<EditUserResponse?> {
            override fun onResponse(call: Call<EditUserResponse?>, response: Response<EditUserResponse?>) {
                if (response.isSuccessful) {
                    data.value = response.body()
                }
                else{
                    val gson = Gson()
                    val type = object : TypeToken<EditUserResponse>() {}.type
                    val errorResponse: EditUserResponse? = gson.fromJson(response.errorBody()!!.charStream(), type)
                    data.value = errorResponse
                }
            }

            override fun onFailure(call: Call<EditUserResponse?>, t: Throwable?) {
                Log.d("Edit user error", t.toString())
            }
        })
        return data
    }

    fun getInternetDataList(mobileNumber: InetDataModel): MutableLiveData<List<InternetDataList>>{
        val data = MutableLiveData<List<InternetDataList>>()
        api.retrofit().getInternetDataList(mobileNumber).enqueue(object :Callback<InternetDataResponse>{
            override fun onResponse(call: Call<InternetDataResponse>, response: Response<InternetDataResponse>) {
                if (response.isSuccessful){
                    data.value = response.body()?.paketData as List<InternetDataList>?
                }
            }
            override fun onFailure(call: Call<InternetDataResponse>, t: Throwable) {
                Log.d("Failed",t.toString())
            }
        })
        return data
    }

    fun getHistoryList(phone: String): MutableLiveData<List<HistoryList>> {
        val data = MutableLiveData<List<HistoryList>>()
        api.retrofit().getHistoryTransaction(phone).enqueue(object :Callback<HistoryResponse>{
            override fun onResponse(call: Call<HistoryResponse>, response: Response<HistoryResponse>) {
                if (response.isSuccessful){
                    data.value = response.body()?.historyList
                    data.value?.forEach {
                        Log.d("History list succcess",it.tanggal)
                    }
                }
            }
            override fun onFailure(call: Call<HistoryResponse>, t: Throwable) {
                Log.d("History list error",t.toString())
            }
        })
        return data
    }

    fun chooseInetPackage(chosenModel: TransactionChosenModel) : MutableLiveData<TransactionResponseChosen>{
        val data = MutableLiveData<TransactionResponseChosen>()
        api.retrofit().chooseInetPackage(chosenModel).enqueue(object :Callback<TransactionResponseChosen>{
            override fun onResponse(call: Call<TransactionResponseChosen>, response: Response<TransactionResponseChosen>) {
                if(response.isSuccessful)
                    data.value = response.body()
            }
            override fun onFailure(call: Call<TransactionResponseChosen>, t: Throwable) {
                Log.d("Failed",t.toString())
            }
        })
        return data
    }

    fun setWallet(walletModel: WalletModel):MutableLiveData<PayNowResponse>{
        val data = MutableLiveData<PayNowResponse>()
        api.retrofit().setWalletTransaction(walletModel).enqueue(object : Callback<PayNowResponse>{
            override fun onResponse(call: Call<PayNowResponse>, response: Response<PayNowResponse>) {
                if(response.isSuccessful) {
                    data.value = response.body()
                    data.value?.status = response.code().toString()
                }
                else{
                    val gson = Gson()
                    val type = object : TypeToken<PayNowResponse>() {}.type
                    val errorResponse: PayNowResponse? = gson.fromJson(response.errorBody()!!.charStream(), type)
                    data.value = errorResponse
                }
            }
            override fun onFailure(call: Call<PayNowResponse>, t: Throwable) {
            }
        })
        return data
    }

    fun setVA(vaModel: VAModel) : MutableLiveData<PayNowResponse>{
        val data = MutableLiveData<PayNowResponse>()
        api.retrofit().setVATransaction(vaModel).enqueue(object : Callback<PayNowResponse>{
            override fun onResponse(call: Call<PayNowResponse>, response: Response<PayNowResponse>) {
                if(response.isSuccessful)
                    data.value = response.body()
                else {
                    val gson = Gson()
                    val type = object : TypeToken<PayNowResponse>() {}.type
                    val errorResponse: PayNowResponse? = gson.fromJson(response.errorBody()!!.charStream(), type)
                    data.value = errorResponse
                }
            }
            override fun onFailure(call: Call<PayNowResponse>, t: Throwable) {
                Log.d("Failed",t.toString())
            }
        })
        return data
    }

    fun uploadImageVA(file : MultipartBody.Part) : MutableLiveData<SignUpResponse>{
        val data = MutableLiveData<SignUpResponse>()
        api.retrofit().uploadVA(file).enqueue(object : Callback<SignUpResponse>{
            override fun onResponse(call: Call<SignUpResponse>, response: Response<SignUpResponse>) {
                if(response.isSuccessful)
                    data.value = response.body()
                else {
                    val gson = Gson()
                    val type = object : TypeToken<SignUpResponse>() {}.type
                    val errorResponse: SignUpResponse? = gson.fromJson(response.errorBody()!!.charStream(), type)
                    data.value = errorResponse
                }
            }
            override fun onFailure(call: Call<SignUpResponse>, t: Throwable) {
                Log.d("Failed",t.toString())
            }
        })
        return data
    }
}
