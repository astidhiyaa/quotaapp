package com.team3.quotapp.repositories

import android.os.AsyncTask
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.team3.quotapp.data.db.HistoryDao
import com.team3.quotapp.data.db.HistoryEntity
import com.team3.quotapp.rest.ApiClient
import com.team3.quotapp.rest.response.EditUserResponse
import com.team3.quotapp.rest.response.HistoryList
import com.team3.quotapp.rest.response.HistoryResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class HistoryRepository(private val historyDao: HistoryDao, private var api: ApiClient) {
    fun insertHistory(history: HistoryEntity) {
        InsertHistoryAsyncTask(historyDao).execute(history)
    }

    fun getAllHistory(): LiveData<List<HistoryEntity>> {
        return historyDao.getAllHistory()
    }

    private class InsertHistoryAsyncTask(val historyDao: HistoryDao) :
        AsyncTask<HistoryEntity, Unit, Unit>() {
        override fun doInBackground(vararg params: HistoryEntity?) {
            historyDao.insert(params[0]!!)
        }
    }

    fun getHistory(mobileNumber:String) : MutableLiveData<List<HistoryList>>{
        var data = MutableLiveData<List<HistoryList>>()
        api.retrofit().getHistoryTransaction(mobileNumber).enqueue(object : Callback<HistoryResponse>{
            override fun onResponse(call: Call<HistoryResponse>, response: Response<HistoryResponse>) {
                if(response.isSuccessful){
                    data.value = response.body()?.historyList
                }
                else{
                    val gson = Gson()
                    val type = object : TypeToken<EditUserResponse>() {}.type
                    val errorResponse: EditUserResponse? = gson.fromJson(response.errorBody()!!.charStream(), type)
//                    data.value = errorResponse
                }
            }
            override fun onFailure(call: Call<HistoryResponse>, t: Throwable) {
                Log.d("Edit user error", t.toString())
            }
        })
        return data
    }
}