package com.team3.quotapp.ui.activities


import android.view.View
import android.view.ViewGroup
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.scrollTo
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.filters.LargeTest
import androidx.test.rule.ActivityTestRule
import androidx.test.runner.AndroidJUnit4
import com.team3.quotapp.R
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.Matchers.`is`
import org.hamcrest.Matchers.allOf
import org.hamcrest.TypeSafeMatcher
import org.hamcrest.core.IsInstanceOf
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@LargeTest
@RunWith(AndroidJUnit4::class)
class RegisterActivity {

    @Rule
    @JvmField
    var mActivityTestRule = ActivityTestRule(SplashActivity::class.java)

    @Test
    fun registerActivity() {
        val materialButton = onView(
            allOf(
                withId(R.id.btn_create_account), withText("Create Account"),
                childAtPosition(
                    childAtPosition(
                        withId(android.R.id.content),
                        0
                    ),
                    2
                ),
                isDisplayed()
            )
        )
        materialButton.perform(click())

        val editText = onView(
            allOf(
                withId(R.id.etPhoneRegister), withText("Phone Number"),
                childAtPosition(
                    allOf(
                        withId(R.id.ll_phone),
                        childAtPosition(
                            IsInstanceOf.instanceOf(android.view.ViewGroup::class.java),
                            3
                        )
                    ),
                    2
                ),
                isDisplayed()
            )
        )
        editText.check(matches(withText("Phone Number")))

        val editText2 = onView(
            allOf(
                withId(R.id.et_full_name), withText("Full Name"),
                childAtPosition(
                    childAtPosition(
                        withId(R.id.layout_name),
                        0
                    ),
                    0
                ),
                isDisplayed()
            )
        )
        editText2.check(matches(withText("Full Name")))

        val editText3 = onView(
            allOf(
                withId(R.id.et_email), withText("E-mail"),
                childAtPosition(
                    childAtPosition(
                        withId(R.id.layout_email),
                        0
                    ),
                    0
                ),
                isDisplayed()
            )
        )
        editText3.check(matches(withText("E-mail")))

        val editText4 = onView(
            allOf(
                withId(R.id.et_email), withText("E-mail"),
                childAtPosition(
                    childAtPosition(
                        withId(R.id.layout_email),
                        0
                    ),
                    0
                ),
                isDisplayed()
            )
        )
        editText4.check(matches(withText("E-mail")))

        val editText5 = onView(
            allOf(
                withId(R.id.et_password), withText("Password"),
                childAtPosition(
                    childAtPosition(
                        withId(R.id.layout_password),
                        0
                    ),
                    0
                ),
                isDisplayed()
            )
        )
        editText5.check(matches(withText("Password")))

        val appCompatEditText = onView(
            allOf(
                withId(R.id.etPhoneRegister),
                childAtPosition(
                    allOf(
                        withId(R.id.ll_phone),
                        childAtPosition(
                            withClassName(`is`("androidx.constraintlayout.widget.ConstraintLayout")),
                            3
                        )
                    ),
                    2
                )
            )
        )
        appCompatEditText.perform(scrollTo(), click())
    }

    private fun childAtPosition(
        parentMatcher: Matcher<View>, position: Int
    ): Matcher<View> {

        return object : TypeSafeMatcher<View>() {
            override fun describeTo(description: Description) {
                description.appendText("Child at position $position in parent ")
                parentMatcher.describeTo(description)
            }

            public override fun matchesSafely(view: View): Boolean {
                val parent = view.parent
                return parent is ViewGroup && parentMatcher.matches(parent)
                        && view == parent.getChildAt(position)
            }
        }
    }
}
